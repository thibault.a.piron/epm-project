import React, { useState } from 'react'

const pug = window.pug
const moment = window.moment

export default props => {
  const showModal = props.showModal === undefined ? false : props.showModal
  const closeButtonHandler = props.closeButtonHandler

  const monthNames = moment.months()

  const defaultFormData = {
    'year': '',
    'month': '',
    'title': '',
    'authors': [''],
    'venue': ''
  }
  const [formData, setFormData] = useState(defaultFormData)

  const handleInputChange = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  const handleAuthorsInputChange = idx => e => {
    const array = formData['authors']
    array[idx] = e.target.value
    setFormData({
      ...formData,
      'authors': array
    })
  }

  const removeAuthorFieldClickHandler = i => () => {
    setFormData({
      ...formData,
      authors: formData['authors'].filter((_, idx) => idx !== i)
    })
  }

  const addAuthorFieldClickHandler = () => {
    setFormData({
      ...formData,
      authors: formData['authors'].concat([''])
    })
  }

  const submitFormHandler = async event => {
    event.preventDefault()
    props.submitFormHandler(formData)
  }

  return pug`
    .modal(className=showModal ? "show-modal" : "")
      .modal-content
        i.fa.fa-window-close.fa-2x.close-button(
          onClick=closeButtonHandler)

        h2 Création d'une publication

        form(onSubmit=submitFormHandler)
          label(for="year") Année:

          input(
            type="number",
            name="year",
            min="1900",
            max="2099",
            step="1",
            value=formData.year,
            onChange=handleInputChange,
            placeholder="Année")

          br

          label(for="month") Mois #{' '}

          select(name="month", value=formData.month, onChange=handleInputChange)
            option(value="")
              | - #{' '} Mois - #{' '}

            each monthName, i in monthNames
              option(key=monthName, value=i)= monthName.charAt(0).toUpperCase() + monthName.slice(1)

          br

          label(for="title") Titre #{':'}

          input(type="text",
            name="title",
            placeholder="Titre",
            value=formData.title,
            onChange=handleInputChange)

          br

          label(for="authors") Auteur #{':'}

          br

          each author, i in formData.authors
            .author-input(key="div" + author)
              input(
                type="text",
                name="authors[]"
                placeholder="Auteur",
                value=author,
                onChange=handleAuthorsInputChange(i))

            if i > 0
              .remove-author(onClick=removeAuthorFieldClickHandler(i))
                i.fa.fa-minus.fa-3x

          .add-author(onClick=addAuthorFieldClickHandler)
            i.fa.fa-plus.fa-3x

          label(for="venue") Revue #{''}

          input(
            type="text",
            name="venue",
            placeholder="Revue",
            value=formData.venue,
            onChange=handleInputChange)

          br

          input(type="submit", value="Création d'une publication")
  `
}

