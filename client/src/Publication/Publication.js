import React, { useState, useEffect } from 'react'

import './Publication.css'

import PublicationTable from './PublicationTable'
import PublicationCreationModal from './PublicationCreationModal'
import Loader from '../Loader/Loader'

const pug = window.pug
const fetch = window.fetch

export default props => {
  const [publications, setPublications] = useState({
    count: 0,
    publications: []
  })

  const [showModal, setShowModal] = useState(false)

  const [pagingOptions, setPagingOptions] = useState({
    'limit': 10,
    'pageNumber': 1,
    'sortBy': 'date',
    'orderBy': 'desc'
  })

  const [loading, setLoading] = useState(true)

  const [errors, setErrors] = useState([])

  const numberOfPages = Math.ceil(publications.count / pagingOptions.limit)

  const previousPageNumber = pagingOptions.pageNumber === 1 ? pagingOptions.pageNumber : pagingOptions.pageNumber - 1
  const nextPageNumber = pagingOptions.pageNumber === numberOfPages ? pagingOptions.pageNumber : pagingOptions.pageNumber + 1

  const getPublications = async () => {
    const searchParams = new URLSearchParams(props.location.search)
    searchParams.set('limit', pagingOptions.limit)
    searchParams.set('page', pagingOptions.pageNumber)
    searchParams.set('sort_by', pagingOptions.sortBy)
    searchParams.set('order_by', pagingOptions.orderBy)
    const response = await fetch('http://localhost:3000/api/publications?' + searchParams.toString(), { 'headers': { 'accept-language': 'fr' } })
    const publications = await response.json()
    setPublications(publications)
    setLoading(false)
  }

  const deletePublicationHandler = async event => {
    const id = event.currentTarget.dataset.id
    await fetch(
      `http://localhost:3000/api/publications/${id}`,
      { method: 'DELETE' })
    await getPublications()
    props.history.push('/publications')
  }

  useEffect(() => {
    getPublications()
  }, [pagingOptions])

  const submitFormHandler = async formData => {
    setLoading(true)
    const response = await fetch(
      'http://localhost:3000/api/publications', {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(formData)
      })

    if (response.status === 201) {
      await getPublications()
      setErrors([])

      const search_params = new URLSearchParams(props.location.search)
      search_params.set('page', 1)
      props.history.push({
        pathname: props.location.pathname,
        search: '?' + search_params.toString()
      })
      setPagingOptions({ ...pagingOptions, 'pageNumber': 1 })
    } else {
      const responseData = await response.json()
      setErrors(responseData.errors)
    }
    setShowModal(false)
    setLoading(false)
  }

  const fieldFilterHandler = e => {
    const search_params = new URLSearchParams(props.location.search)
    search_params.set('sort_by', e.target.value)
    props.history.push({
      pathname: props.location.pathname,
      search: '?' + search_params.toString()
    })
    setPagingOptions({ ...pagingOptions, 'sortBy': e.target.value })
  }

  const filterAscValueHandler = e => {
    const search_params = new URLSearchParams(props.location.search)
    search_params.set('order_by', e.target.value)
    props.history.push({
      pathname: props.location.pathname,
      search: '?' + search_params.toString()
    })
    setPagingOptions({ ...pagingOptions, 'orderBy': e.target.value })
  }

  const elementsPerPageHandler = e => {
    const search_params = new URLSearchParams(props.location.search)
    search_params.set('limit', e.target.value)
    search_params.set('page', 1)
    props.history.push({
      pathname: props.location.pathname,
      search: '?' + search_params.toString()
    })
    setPagingOptions({ ...pagingOptions, 'limit': Number(e.target.value), 'pageNumber': 1 })
  }

  const paginationClickHandler = e => {
    const search_params = new URLSearchParams(props.location.search)
    search_params.set('limit', pagingOptions.limit)
    search_params.set('page', e.target.dataset.pagenumber)
    props.history.push({
      pathname: props.location.pathname,
      search: '?' + search_params.toString()
    })
    setPagingOptions({ ...pagingOptions, 'pageNumber': Number(e.target.dataset.pagenumber) })
  }

  return pug`
    .loading-container
      if loading
        Loader(loading=loading)

      else
        h2 Publications

        if errors.length > 0
          .errors
            p Il y a des erreurs dans la soumission du formulaire. Veuillez les corriger.
            ul
              each err, i in errors
                li(key="error" + i)= err

        button.trigger(onClick=() => setShowModal(!showModal)) Ajouter une publication

        PublicationCreationModal(
          showModal=showModal,
          closeButtonHandler=() => setShowModal(false),
          submitFormHandler=submitFormHandler)

        p
          | Trié par: #{''}
          select#fieldFilterSection(defaultValue=pagingOptions.sortBy, onChange=fieldFilterHandler)
            each option in ['date', 'title']
              option(key="option" + option, value=option)= option

        p
          | Ordonner par: #{''}
          select#filterAscValueSection(defaultValue=pagingOptions.orderBy, onChange=filterAscValueHandler)
            option(value="desc") décroissant
            option(value="asc") croissant

        PublicationTable(
          publications=publications.publications,
          deletePublicationHandler=deletePublicationHandler)

        .pagination
          a.pagination-link(data-pagenumber=previousPageNumber, onClick=paginationClickHandler) &laquo;
          each page in [...Array(numberOfPages).keys()].map(p => p + 1)
            a.pagination-link(
              key="pagination-link-" + page,
              className=page == pagingOptions.pageNumber ? "active" : "",
              data-pagenumber=page,
              onClick=paginationClickHandler)= page

          a.pagination-link(data-pagenumber=nextPageNumber, onClick=paginationClickHandler) &raquo;

          p
            | Afficher
            select#elementsPerPageSection(defaultValue=pagingOptions.limit, onChange=elementsPerPageHandler)
              each value in [10, 20, 30, 50, 100]
                option(key="option" + value, value=value)= value

            | résultats par page
  `
}
