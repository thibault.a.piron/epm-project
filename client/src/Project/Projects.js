import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import './Projects.css'

import Loader from '../Loader/Loader'

const pug = window.pug
const fetch = window.fetch

const ProjectDescription = props => {
  const project = props.project

  return pug`
    li
      span
        = project.student
        | #{', '}

      Link(to="/projects/" + project._id)= project.title
      footer.meta
        p
          | Directeur(e): #{''}
          = project.supervisor

        if project.cosupervisor
          p
            | Co-directeur(e)(s): #{''}
            = project.cosupervisor
  `
}

export default () => {
  const [projects, setProjects] = useState([])

  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const getProjects = async () => {
      const response = await fetch('http://localhost:3000/api/projects', { 'headers': { 'accept-language': 'fr' } })
      const projects = await response.json()
      setProjects(projects)
      setLoading(false)
    }
    getProjects()
  }, [loading])

  const currentProjects = projects
    .filter(p => p.current)
    .sort((p1, p2) => p1.year < p2.year ? 1 : p1.year > p2.year ? -1 : 0)
  const pastProjects = projects
    .filter(p => !p.current)
    .sort((p1, p2) => p1.year < p2.year ? 1 : p1.year > p2.year ? -1 : 0)

  return pug`
    .loading-container
      if loading
        Loader(loading=loading)

      else
        h1 Projets en cours

        if currentProjects.length === 0
          p Aucuns projets en cours

        else
          ul.projects
            each project in currentProjects
              ProjectDescription(key=project._id, project=project)

        h1 Projets passés

        if pastProjects.length === 0
          p Aucuns projets passés

        else
          ul.projects
            each project in pastProjects
              ProjectDescription(key=project._id, project=project)
  `
}
