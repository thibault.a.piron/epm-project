import React, { useState, useEffect } from 'react'

import PublicationTable from '../Publication/PublicationTable'
import Loader from '../Loader/Loader'

import './Project.css'

const pug = window.pug
const fetch = window.fetch

export default props => {
  const [project, setProject] = useState({})

  const [publications, setPublications] = useState([])

  const [loading, setLoading] = useState(true)

  const url = `http://localhost:3000/api/projects/${props.match.params.id}`
  const getProject = async () => {
    const response = await fetch(url, { 'headers': { 'accept-language': 'fr' } })
    const data = await response.json()
    setProject(data.project)
    setPublications(data.publications)
    setLoading(false)
  }

  const deletePublicationHandler = async event => {
    const id = event.currentTarget.dataset.id
    await fetch(
      `http://localhost:3000/api/publications/${id}`,
      { method: 'DELETE' })
    await getProject()
    props.history.push('/projects/props.match.params.id')
  }

  useEffect(() => {
    getProject()
  }, [loading])

  return pug`
    .loading-container
      if loading
        Loader(loading=loading)

      else
        if project && Object.keys(project).length !== 0
          h1= project.title

          section.description
            footer.meta
              p
                | Étudiant: #{''}
                = project.student

              p
                | Directeur(e): #{''}
                = project.supervisor

              if project.cosupervisor
                p
                  | Co-directeur(e)(s): #{''}
                  = project.cosupervisor

            div
              each paragraph, i in project.description.split('\n')
                p(key=i)= paragraph

            if project.thesisUrl
              p
                | Pour plus d'informations, #{''}
                a(href=project.thesisUrl) cliquez ici

          if publications.length > 0
            h2 Publications
            PublicationTable(
              publications=publications,
              deletePublicationHandler=deletePublicationHandler)
  `
}
